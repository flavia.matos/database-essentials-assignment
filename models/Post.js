const { Schema, model } = require('mongoose');

const PostSchema = Schema({
    text: {
        type: String,
        required: true,
    },
});

module.exports = model('Posts', PostSchema);
