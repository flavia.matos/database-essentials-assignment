const winston = require('winston');
const logstash = require('winston-logstash-transport');
require('dotenv/config');

const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.errors({ stack: true }),
        winston.format.timestamp(),
        winston.format.json()
    ),
    transports: [
        new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
        new winston.transports.File({ filename: './logs/info.log', level: 'info' }),
        new logstash.LogstashTransport({
            host: process.env.ELASTIC_HOST,
            port: process.env.ELASTIC_PORT
        }),
    ],
});

module.exports = logger;