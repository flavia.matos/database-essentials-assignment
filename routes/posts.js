const express = require('express');
const Post = require('../models/Post');
const logger = require('../logs/logger');

const router = express.Router();

router.get('/list', async (req, res) => {
    try {
        const posts = await Post.find();
        res.json(posts);
        logger.info('Successful execution of request: list posts');
    } catch(err) {
        res.status(400).send(err.message);
        logger.error(err);
    }
});

router.post('/add', async (req, res) => {
    const { text } = req.body;
    const post = new Post({ text });
    try {
        const savedPost = await post.save();
        res.json(savedPost);
        logger.info('Successful execution of request: add new post');
    } catch (err) {
        res.status(400).send(err.message);
        logger.error(err);
    }
});

router.patch('/edit/:postId', async (req, res) => {
    try {
        const { postId:_id } = req.params;
        const { text } = req.body;
        const editedPost = await Post.updateOne({ _id }, { $set: { text } });
        res.json(editedPost);
        logger.info('Successful execution of request: edit a post');
    } catch (err) {
        res.status(400).send(err.message);
        logger.error(err);
    }
});

module.exports = router;
